package nipon.com.recyclerviewusingmvvmandretrofit.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import nipon.com.recyclerviewusingmvvmandretrofit.CustomAdapter.CommentAdapter;
import nipon.com.recyclerviewusingmvvmandretrofit.CustomAdapter.PostAdapter;
import nipon.com.recyclerviewusingmvvmandretrofit.Model.Comment;
import nipon.com.recyclerviewusingmvvmandretrofit.R;
import nipon.com.recyclerviewusingmvvmandretrofit.ViewModel.CommentViewModel;
import nipon.com.recyclerviewusingmvvmandretrofit.databinding.ActivityCommentBinding;

public class CommentActivity extends AppCompatActivity {

    private ActivityCommentBinding binding;
    private CommentAdapter commentAdapter;
    private CommentViewModel commentViewModel;

    private List<Comment> commentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_comment);

        commentViewModel = ViewModelProviders.of(this).get(CommentViewModel.class);

        initRecyclerView();

        getAllComments();
    }


    private void initRecyclerView() {

        commentList = new ArrayList<>();
        binding.commentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(commentList,this);
        binding.commentRecyclerView.setAdapter(commentAdapter);

    }


    private void getAllComments() {
        commentViewModel.getAllComment().observe(this, new Observer<List<Comment>>() {
            @Override
            public void onChanged(@Nullable List<Comment> comments) {
                commentList.clear();
                if (comments!=null){
                    commentList.addAll(comments);
                    commentAdapter.notifyDataSetChanged();
                }
                getIsLoaded();
            }
        });
    }

    private void getIsLoaded() {
        commentViewModel.getIsLoaded().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean==true){
                    binding.commentProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }
}
